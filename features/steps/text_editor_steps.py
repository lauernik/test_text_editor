from behave import given, when, then
from features.pageobject.notepad_page import NotepadPage
from features.pageobject.notepadPlus_page import NotepadPlusPage


@given('открыли программу notepad')
def step_impl(context):
    context.app = NotepadPage('notepad.exe')
    context.app.start_editor()


@when('ввели "{text}" в {text_editor}')
def step_impl(context, text, text_editor):
    context.app.input_text(text)


@then('экстренно завершили работу {text_editor}')
def step_impl(context, text_editor):
    context.app.kill_editor()


@when('заново открыли {text_editor}')
def step_impl(context, text_editor):
    context.app.start_editor()


@then('проверили, что текст в {text_editor} сохранился')
def step_impl(context, text_editor):
    context.app.check_saved_text()


# Шаг для теста Notepad++
@given('открыли программу notepad++')
def step_impl(context):
    context.app = NotepadPlusPage("C:/Program Files/Notepad++/notepad++.exe")
    context.app.start_editor()
