from pywinauto.application import Application


class TextNotFound(Exception):
    pass


class BaseTextEditor:
    def __init__(self, text_editor):
        self.text_editor = text_editor
        self.app = Application()

    def start_editor(self):
        self.app.start(self.text_editor)

    def kill_editor(self):
        """
        Экстренное завершение работы редактора
        Только при soft=True текст в Notepad++ сохранится
        """
        self.app.kill(soft=True)
