from .text_editor_basePage import BaseTextEditor, TextNotFound


class NotepadPage(BaseTextEditor):
    def input_text(self, text):
        self.inputed_text = text
        self.app.Notepad.Edit.type_keys(text, with_spaces=True)

    def check_saved_text(self):
        text_from_editor = self.app.Notepad.Edit.window_text()
        self.kill_editor()
        if text_from_editor != self.inputed_text:
            raise TextNotFound(f'Текст: "{self.inputed_text}" не найден')
