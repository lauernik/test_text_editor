from .text_editor_basePage import BaseTextEditor, TextNotFound
from pywinauto.keyboard import send_keys


class NotepadPlusPage(BaseTextEditor):
    def input_text(self, text):
        self.inputed_text = text
        self.app.Notepad.Scintilla.type_keys(text, with_spaces=True)

    def check_saved_text(self):
        text_from_editor = self.app.Notepad.Scintilla.window_text()
        send_keys('^a{BACKSPACE}')  # Очистка ввода, для последующих тестов
        self.kill_editor()
        if text_from_editor != self.inputed_text:
            raise TextNotFound(f'Текст: "{self.inputed_text}" не найден')
